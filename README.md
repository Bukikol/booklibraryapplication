Wizer BookLibrary API
The project is in the "main" branch
Base URL : /api/v1
The below are endpoints available

/create_book
/{id}/edit_book
/book_update
/{id}/delete_book
/book_list
/create_category
/{id}/edit_category
/update_category
/{id}/delete_category
/category_list

A category entity can only be deleted if its not mapped with the Book entity
