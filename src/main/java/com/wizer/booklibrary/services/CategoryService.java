package com.wizer.booklibrary.services;

import com.wizer.booklibrary.dto.CategoryDTO;
import com.wizer.booklibrary.models.Category;

import java.util.List;

public interface CategoryService {

    Category addCategory(CategoryDTO categoryDTO);
    Category editCategory(CategoryDTO categoryDTO);
    List<Category> getCategories();
    void deleteCategory(Long id);
    Category getCategoryById(Long id);
}
