package com.wizer.booklibrary.services.Implementation;

import com.wizer.booklibrary.dto.CategoryDTO;
import com.wizer.booklibrary.models.Category;
import com.wizer.booklibrary.repositories.CategoryRepository;
import com.wizer.booklibrary.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public Category addCategory(CategoryDTO categoryDTO) {
        Category category=new Category();
        category.setCategoryName(categoryDTO.getCategoryName());
        return categoryRepository.save(category);

    }

    @Override
    public Category editCategory(CategoryDTO categoryDTO) {
        Category category=categoryRepository.getById(categoryDTO.getCategoryId());
        category.setCategoryName(categoryDTO.getCategoryName());
        return categoryRepository.save(category);
    }

    @Override
    public List<Category> getCategories() {
        return categoryRepository.findAll();
    }

    @Override
    public void deleteCategory(Long id) {
        Category category=categoryRepository.getById(id);
        categoryRepository.delete(category);

    }

    @Override
    public Category getCategoryById(Long id) {
        return categoryRepository.getById(id);
    }
}
