package com.wizer.booklibrary.services.Implementation;

import com.wizer.booklibrary.dto.BookDTO;
import com.wizer.booklibrary.models.Book;
import com.wizer.booklibrary.repositories.BookRepository;
import com.wizer.booklibrary.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    BookRepository bookRepository;

    @Override
    public Book addBook(BookDTO bookDTO) {
       Book book=new Book();
       book.setAuthor(bookDTO.getAuthor());
      book.setIsbn(bookDTO.getIsbn());
       book.setPublisher(bookDTO.getPublisher());
       book.setTitle(bookDTO.getTitle());
       book.setCountry(bookDTO.getCountry());
       book.setCategory(bookDTO.getCategory());
       return bookRepository.save(book);
    }

    @Override
    public Book editBook(BookDTO bookDTO) {
       Book book=bookRepository.getById(bookDTO.getId());
        book.setAuthor(bookDTO.getAuthor());
        book.setIsbn(bookDTO.getIsbn());
        book.setPublisher(bookDTO.getPublisher());
        book.setTitle(bookDTO.getTitle());
        book.setCountry(bookDTO.getCountry());
        book.setCategory(bookDTO.getCategory());
        return bookRepository.save(book);
    }

    @Override
    public List<Book> getAllBooks() {
       return bookRepository.findAll();
    }

    @Override
    public Book getBookById(Long id) {
        return bookRepository.getById(id);
    }

    @Override
    public void deleteBook(Long id) {
     Book book=bookRepository.getById(id);
        bookRepository.delete(book);

    }
}
