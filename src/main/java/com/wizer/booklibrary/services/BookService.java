package com.wizer.booklibrary.services;

import com.wizer.booklibrary.dto.BookDTO;
import com.wizer.booklibrary.models.Book;

import java.util.List;

public interface BookService {

    Book addBook(BookDTO bookDTO);

    Book editBook(BookDTO bookDTO);
    List<Book> getAllBooks();

    Book getBookById(Long id);
    void deleteBook(Long id);

}
