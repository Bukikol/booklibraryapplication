package com.wizer.booklibrary.controllers;

import com.wizer.booklibrary.dto.CategoryDTO;
import com.wizer.booklibrary.models.Category;
import com.wizer.booklibrary.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @PostMapping("/create_category")
    public Category add_category(@RequestBody CategoryDTO categoryDTO){
        return categoryService.addCategory(categoryDTO);
    }
    @GetMapping("/{id}/edit_category")
    public Category  edit_category(@PathVariable Long id){
        return categoryService.getCategoryById(id);

    }
    @PostMapping("/update_category")
    public Category  update_category(@RequestBody CategoryDTO categoryDTO){
        return categoryService.editCategory(categoryDTO);

    }

    @GetMapping("/{id}/delete_category")
    public void deleteABook(@PathVariable Long id){
        categoryService.deleteCategory(id);
    }


    @GetMapping("/category_list")
    public List<Category> getCategories(){
        List<Category> categories=categoryService.getCategories();
        return categories;
    }
}
