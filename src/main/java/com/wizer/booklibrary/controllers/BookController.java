package com.wizer.booklibrary.controllers;

import com.wizer.booklibrary.dto.BookDTO;
import com.wizer.booklibrary.models.Book;
import com.wizer.booklibrary.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class BookController {

    @Autowired
    BookService bookService;
    @PostMapping("/create_book")
    public Book create_Book(@RequestBody BookDTO bookDTO){
        return bookService.addBook(bookDTO);
    }

    @GetMapping("/{id}/edit_book")
    public Book  edit_book(@PathVariable Long id){
        return bookService.getBookById(id);

    }
    @PostMapping("/book_update")
    public Book  update_book(@RequestBody BookDTO bookDTO){
        return bookService.editBook(bookDTO);

    }

    @GetMapping("/{id}/delete_book")
    public void delete_Book(@PathVariable Long id){
        bookService.deleteBook(id);
    }


    @GetMapping("/book_list")
    public List<Book> getBooks(){
        List<Book> books=bookService.getAllBooks();
        return books;
    }
}

