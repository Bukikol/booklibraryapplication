package com.wizer.booklibrary.dto;

import com.wizer.booklibrary.models.Category;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BookDTO {
    private Long id;
    private String title;
    private String author;
    private String country;
    private String isbn;
    private String publisher;
    private Category category;
}
